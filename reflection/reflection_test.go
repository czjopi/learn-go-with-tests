package main

import (
	"reflect"
	"testing"
)

type Property struct {
	City string
	Age  int
}

func TestWalk(t *testing.T) {
	cases := []struct {
		Name          string
		Input         interface{}
		ExpectedCalls []string
	}{
		{
			"Struct with one field",
			struct {
				Name string
			}{"Chris"},
			[]string{"Chris"},
		},
		{
			"Struct with two fields",
			struct {
				Name string
				City string
			}{"Chris", "London"},
			[]string{"Chris", "London"},
		},
		{
			"Struct with not only string fields",
			struct {
				Name string
				City string
				Age  int
			}{"Chris", "London", 30},
			[]string{"Chris", "London"},
		},
		{
			"Nested struct",
			struct {
				Name string
				Property
			}{"Chris", struct {
				City string
				Age  int
			}{"London", 30}},
			[]string{"Chris", "London"},
		},
		{
			"Pointers",
			&struct {
				Name string
				Property
			}{"Chris", struct {
				City string
				Age  int
			}{"London", 30}},
			[]string{"Chris", "London"},
		},
		{
			"Slices",
			[]Property{
				{"London", 33},
				{"Brighton", 24},
			},
			[]string{"London", "Brighton"},
		},
		{
			"Arrays",
			[2]Property{
				{"London", 33},
				{"Brighton", 24},
			},
			[]string{"London", "Brighton"},
		},
	}

	for _, tt := range cases {
		t.Run(tt.Name, func(t *testing.T) {
			var got []string

			walk(tt.Input, func(input string) {
				got = append(got, input)
			})

			if !reflect.DeepEqual(got, tt.ExpectedCalls) {
				t.Errorf("got %v wanted %v", got, tt.ExpectedCalls)
			}
		})
	}
}

func TestWalkMaps(t *testing.T) {
	aMap := map[int]string{
		0: "foo",
		1: "bar",
	}

	var got []string

	walk(aMap, func(input string) {
		got = append(got, input)
	})

	assertContains(t, got, "foo")
	assertContains(t, got, "bar")
}

func assertContains(t testing.TB, m []string, s string) {
	t.Helper()

	var contains bool
	for _, value := range m {
		if value == s {
			contains = true
		}
	}

	if !contains {
		t.Errorf("expected %+v contain %q, but it didn't", m, s)
	}
}

func TestChannel(t *testing.T) {
	aChannel := make(chan Property)

	go func() {
		aChannel <- Property{
			City: "London",
			Age:  30,
		}
		aChannel <- Property{
			City: "Prague",
			Age:  45,
		}
		close(aChannel)
	}()

	var got []string
	want := []string{"London", "Prague"}

	walk(aChannel, func(input string) {
		got = append(got, input)
	})

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v want %v", got, want)
	}
}

func TestFunction(t *testing.T) {
	aFunc := func() (Property, Property) {
		return Property{"London", 33}, Property{"Brighton", 45}
	}

	var got []string
	want := []string{"London", "Brighton"}

	walk(aFunc, func(input string) {
		got = append(got, input)
	})

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %v want %v", got, want)
	}
}
