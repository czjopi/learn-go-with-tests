package main

import (
	"fmt"
	"net/http"
	"time"
)

const timeoutTenSecond = 10 * time.Second

// Racer compares response time of url1, url2 and returns fastest one,
// timing out after 10s.
func Racer(url1, url2 string) (string, error) {
	return ConfigurableRacer(url1, url2, timeoutTenSecond)
}

// ConfigurableRacer compares response time of url1, url2 and returns fastest one.
func ConfigurableRacer(url1, url2 string, duration time.Duration) (string, error) {
	select {
	case <-ping(url1):
		return url1, nil
	case <-ping(url2):
		return url2, nil
	case <-time.After(duration):
		return "", fmt.Errorf("timed out waiting for %s and %s", url1, url2)
	}
}

// Ping will send a signal to a channel once we have completed http.Get(url)
// and returns channel
func ping(url string) chan struct{} {
	ch := make(chan struct{})
	go func() {
		http.Get(url)
		close(ch)
	}()
	return ch
}
