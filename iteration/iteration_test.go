package iteration

import (
	"fmt"
	"testing"
)

var repeatTests = []struct {
	char  string
	count int
	want  string
}{
	{"a", 5, "aaaaa"},
	{"", 0, ""},
	{"", 2, ""},
	{"123 ", 2, "123 123 "},
}

func TestRepeat(t *testing.T) {
	for _, a := range repeatTests {
		got := Repeat(a.char, a.count)
		want := a.want
		if got != want {
			t.Errorf("got: %q, want: %q", got, want)
		}
	}
}

func BenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("b", 5)
	}
}

func BenchmarkRepeat_empty(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("", 5)
	}
}

func ExampleRepeat() {
	fmt.Println(Repeat("x", 3))
	// Output: xxx
}
