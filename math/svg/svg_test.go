package svg_test

import (
	"bytes"
	. "clockface/svg"
	"encoding/xml"
	"testing"
	"time"
)

// struct for unmarshalling xml
// [online](https://www.onlinetool.io/xmltogo/) version
// of [zek](https://github.com/miku/zek) used for conversion
type SVG struct {
	XMLName xml.Name `xml:"svg"`
	Xmlns   string   `xml:"xmlns,attr"`
	Width   string   `xml:"width,attr"`
	Height  string   `xml:"height,attr"`
	ViewBox string   `xml:"viewBox,attr"`
	Version string   `xml:"version,attr"`
	Circle  Circle   `xml:"circle"`
	Line    []Line   `xml:"line"`
}

type Circle struct {
	Cx float64 `xml:"cx,attr"`
	Cy float64 `xml:"cy,attr"`
	R  float64 `xml:"r,attr"`
}

type Line struct {
	X1 float64 `xml:"x1,attr"`
	Y1 float64 `xml:"y1,attr"`
	X2 float64 `xml:"x2,attr"`
	Y2 float64 `xml:"y2,attr"`
}

func TestSVGWriteSecondHand(t *testing.T) {
	tests := []struct {
		name string
		time time.Time
		want Line
	}{
		{
			"Second at midnight",
			simpleTime(0, 0, 0),
			Line{150, 150, 150, 60},
		},
		{
			"Second at 30",
			simpleTime(0, 0, 30),
			Line{150, 150, 150, 240},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			b := bytes.Buffer{}
			Write(&b, tt.time)

			svg := SVG{}
			xml.Unmarshal(b.Bytes(), &svg)

			if !containsLine(tt.want, svg.Line) {
				t.Errorf("Expected to find the line: %+v in the SVG output, but found: %+v", tt.want, svg.Line)

			}
		})
	}
}

func TestSVGWriteMinuteHand(t *testing.T) {
	tests := []struct {
		name string
		time time.Time
		line Line
	}{
		{
			"Minutes at midnight",
			simpleTime(0, 0, 0),
			Line{150, 150, 150, 70},
		},
		{
			"Minutes at 30",
			simpleTime(0, 30, 0),
			Line{150, 150, 150, 230},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := bytes.Buffer{}
			Write(&b, tt.time)

			svg := SVG{}
			xml.Unmarshal(b.Bytes(), &svg)

			if !containsLine(tt.line, svg.Line) {
				t.Errorf("Expected to find the minute hand line %+v, in the SVG lines %+v", tt.line, svg.Line)
			}
		})
	}
}

func TestSVGWriteHourHand(t *testing.T) {
	tests := []struct {
		name string
		time time.Time
		line Line
	}{
		{
			"Hours at midnight",
			simpleTime(0, 0, 0),
			Line{150, 150, 150, 100},
		},
		{
			"Hours at 21",
			simpleTime(21, 0, 0),
			Line{150, 150, 100, 150},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			b := bytes.Buffer{}
			Write(&b, tt.time)

			svg := SVG{}
			xml.Unmarshal(b.Bytes(), &svg)

			if !containsLine(tt.line, svg.Line) {
				t.Errorf("Expected to find the hour hand line %+v, in the SVG lines %+v", tt.line, svg.Line)
			}
		})
	}
}

func containsLine(x Line, y []Line) bool {
	for _, line := range y {
		if x == line {
			return true
		}
	}
	return false
}

func simpleTime(hour, minute, second int) time.Time {
	return time.Date(1993, time.January, 15, hour, minute, second, 0, time.UTC)
}
