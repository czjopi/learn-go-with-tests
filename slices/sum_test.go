package main

import (
	"reflect"
	"testing"
)

func TestSum(t *testing.T) {
	t.Run("collection of any size", func(t *testing.T) {
		numbers := []int{1, 2, 3, 4, 5}

		got := Sum(numbers)
		want := 15
		if got != want {
			t.Errorf("got: %d, want: %d, given: %v", got, want, numbers)
		}
	})
}

func TestSumAll(t *testing.T) {
	t.Run("collection of ints", func(t *testing.T) {
		n1 := []int{1, 2, 3, 4, 5}
		n2 := n1
		got := SumAll(n1, n2)
		want := []int{15, 15}
		if !reflect.DeepEqual(got, want) {
			t.Errorf("got: %d, want: %d, given: (%v, %v)", got, want, n1, n2)
		}
	})
	t.Run("more collections of ints", func(t *testing.T) {
		numbers := []int{1, 2, 3, 4, 5, 6}
		got := SumAll(numbers)
		want := []int{21}
		if !reflect.DeepEqual(got, want) {
			t.Errorf("got: %d, want: %d, given: %v", got, want, numbers)
		}
	})
}

func TestSumAllTails(t *testing.T) {
	checkSums := func(t testing.TB, got, want []int) {
		t.Helper()
		if !reflect.DeepEqual(got, want) {
			t.Errorf("got: %d, want: %d", got, want)
		}
	}
	t.Run("sums tails of ints collection", func(t *testing.T) {
		numbers := []int{1, 2, 3, 4, 5}
		got := SumAllTails(numbers)
		want := []int{14}
		checkSums(t, got, want)
	})
	t.Run("sums tails of ints collection but one is empty", func(t *testing.T) {
		n1 := []int{1, 2, 3, 4, 5}
		n2 := []int{}
		got := SumAllTails(n1, n2)
		want := []int{14, 0}
		checkSums(t, got, want)
	})
}
