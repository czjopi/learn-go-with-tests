package integers

import (
	"fmt"
	"testing"
)

var addTests = []struct {
	x, y, sum int
}{
	{2, 2, 4},
	{1, 2, 3},
	{0, -2, -2},
	{-100, 300, 200},
}

func TestAdd(t *testing.T) {
	for _, a := range addTests {
		got := Add(a.x, a.y)
		want := a.sum
		if got != want {
			t.Errorf("got: %d, want: %d", got, want)
		}
	}
}

func ExampleAdd() {
	sum := Add(2, 7)
	fmt.Println(sum)
	// Output: 9
}
