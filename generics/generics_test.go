package generics

import "testing"

func TestAssertFunctions(t *testing.T) {
	t.Run("assert on integers", func(t *testing.T) {
		assertEqual(t, 1, 1)
		assertNotEqual(t, 1, 2)
	})

	t.Run("assert on strings", func(t *testing.T) {
		assertEqual(t, "hello", "hello")
		assertNotEqual(t, "hello1", "hello2")
	})

	//assertEqual(t, 1, "1") // Uncomment this line to see the compilation error
}

func TestStack(t *testing.T) {
	t.Run("test stack of ints", func(t *testing.T) {
		myStackOfInts := new(Stack[int])

		// new stack is empty
		assertTrue(t, myStackOfInts.IsEmpty())

		// insert an item and test if stack is not empty
		myStackOfInts.Push(321)
		assertFalse(t, myStackOfInts.IsEmpty())

		// add another item and pop it back
		myStackOfInts.Push(456)
		v, b := myStackOfInts.Pop()
		assertEqual(t, v, 456)
		assertTrue(t, b)
		v, b = myStackOfInts.Pop()

		// remove last item and check stack is empty
		assertEqual(t, v, 321)
		assertTrue(t, myStackOfInts.IsEmpty())

		// pop an empty stack
		_, b = myStackOfInts.Pop()
		assertFalse(t, b)
	})
	t.Run("interface stack dx is horrid", func(t *testing.T) {
		myStackOfInts := new(Stack[int])

		myStackOfInts.Push(1)
		myStackOfInts.Push(2)
		firstNum, _ := myStackOfInts.Pop()
		secondNum, _ := myStackOfInts.Pop()
		assertEqual(t, firstNum+secondNum, 3)
	})
}

func assertEqual(t *testing.T, got, want interface{}) {
	t.Helper()
	if got != want {
		t.Errorf("assertEqual, but %#v != %#v", got, want)
	}
}

func assertNotEqual[T comparable](t *testing.T, got, want T) {
	t.Helper()
	if got == want {
		t.Errorf("assertNotEqual, but %#v == %#v", got, want)
	}
}

func assertTrue(t *testing.T, got bool) {
	t.Helper()
	if !got {
		t.Errorf("got %v, want true", got)
	}
}

func assertFalse(t *testing.T, got bool) {
	t.Helper()
	if got {
		t.Errorf("got %v, want false", got)
	}
}
