package shape

import "testing"

func TestPerimeter(t *testing.T) {
	rectangle := Rectangle{10.0, 10.0}
	got := Perimeter(rectangle)
	want := 40.0
	if got != want {
		t.Errorf("got: %.2f, want: %.2f", got, want)
	}
}

func TestArea(t *testing.T) {
	var tests = []struct {
		name    string
		hasArea float64
		shape   Shape
	}{
		{name: "rectangle", hasArea: 200.0, shape: Rectangle{10.0, 20.0}},
		{name: "circle", hasArea: 314.1592653589793, shape: Circle{10.0}},
		{name: "triangle", hasArea: 36.0, shape: Triangle{12, 6}},
	}
	for _, tt := range tests {
		tt := tt
		t.Run(tt.name, func(t *testing.T) {
			actual := (tt.shape.Area())
			if actual != tt.hasArea {
				t.Errorf("(%#v): expected %g, actual %g", tt.shape, tt.hasArea, actual)
			}

		})
	}
}
