package clockface

import (
	"fmt"
	"io"
	"math"
	"time"
)

const secondHandLength = 90
const minuteHandLength = 80
const hoursHandLength = 50
const clockCentreX = 150
const clockCentreY = 150

// Point represents two-dimensional Cartesian coordinate
type Point struct {
	X float64
	Y float64
}

// SecondHand vector unit of analogue clock
func SecondHand(w io.Writer, t time.Time) {
	p := makeHand(SecondHandPoint(t), secondHandLength)
	fmt.Fprintf(w, `<line x1="150" y1="150" x2="%.2f" y2="%.2f" style="fill:none;stroke:#f00;stroke-width:3px;"/>`, p.X, p.Y)
}

func MinuteHand(w io.Writer, t time.Time) {
	p := makeHand(MinuteHandPoint(t), minuteHandLength)
	fmt.Fprintf(w, `<line x1="150" y1="150" x2="%.2f" y2="%.2f" style="fill:none;stroke:#000;stroke-width:5px;"/>`, p.X, p.Y)
}

func HoursHand(w io.Writer, t time.Time) {
	p := makeHand(HoursHandPoint(t), hoursHandLength)
	fmt.Fprintf(w, `<line x1="150" y1="150" x2="%.2f" y2="%.2f" style="fill:none;stroke:#000;stroke-width:5px;"/>`, p.X, p.Y)
}

func makeHand(p Point, l float64) Point {
	p = Point{p.X * l, p.Y * l}                       // Scale it to the length of the clock handle
	p = Point{p.X, -p.Y}                              // Flip it over x-axis, SVG have an origin int the top left corner
	p = Point{p.X + clockCentreX, p.Y + clockCentreY} // Move it to the clock center
	return p
}

func SecondHandPoint(t time.Time) Point {
	return angleToPoint(secondsInRadians(t))
}

func MinuteHandPoint(t time.Time) Point {
	return angleToPoint(minutesInRadians(t))
}

func HoursHandPoint(t time.Time) Point {
	return angleToPoint(hoursInRadians(t))
}

func angleToPoint(radians float64) Point {
	x := math.Sin(radians)
	y := math.Cos(radians)
	return Point{x, y}
}

// secondsInRadians returns angle of analogue clock hand in radians
func secondsInRadians(t time.Time) float64 {
	seconds := float64(t.Second())
	return math.Pi / (30 / seconds)
}

func minutesInRadians(t time.Time) float64 {
	minutes := float64(t.Minute())
	return math.Pi / (30 / minutes)
}

func hoursInRadians(t time.Time) float64 {
	return (minutesInRadians(t) / 12) + (math.Pi / (6 / float64(t.Hour()%12)))
}
