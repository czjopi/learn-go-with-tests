package numerals

import (
	"strings"
)

// RomanNumeral representation of Roman numeral
type RomanNumeral struct {
	Value  int
	Symbol string
}

type RomanNumerals []RomanNumeral

// ValueOf returns numerical value of Roman symbol
func (r RomanNumerals) ValueOf(symbol ...byte) int {
	for _, s := range r {
		if s.Symbol == string(symbol) {
			return s.Value
		}
	}
	return 0
}

// AllRomanNumerals array of Roman numbers
var AllRomanNumerals = RomanNumerals{
	{1000, "M"},
	{900, "CM"},
	{500, "D"},
	{400, "CD"},
	{100, "C"},
	{90, "XC"},
	{50, "L"},
	{40, "XL"},
	{10, "X"},
	{9, "IX"},
	{5, "V"},
	{4, "IV"},
	{1, "I"},
}

// ConvertToRoman returns Roman representation of Arabic number
func ConvertToRoman(arabic int) string {
	str := strings.Builder{}

	for _, num := range AllRomanNumerals {
		for arabic >= num.Value {
			str.WriteString(num.Symbol)
			arabic -= num.Value
		}
	}

	return str.String()
}

// ConvertToArabic returns Arabic representation of Roman number
func ConvertToArabic(roman string) int {
	num := 0

	for index := 0; index < len(roman); index++ {
		symbol := roman[index]
		if index+1 < len(roman) {
			// get value of two character symbol
			possibleValue := AllRomanNumerals.ValueOf(symbol, roman[index+1])
			if possibleValue != 0 {
				num += possibleValue
				index++ // skip next symbol because we have two character symbol
				continue
			}
		}
		num += AllRomanNumerals.ValueOf(symbol)
	}

	return num
}
