package main

import (
	"testing"
)

var helloTests = []struct {
	title string
	name  string
	lang  string
	want  string
}{
	{"empty string defaults to 'World'", "", "", "Hello, World"},
	{"say hello to people", "Chris", "", "Hello, Chris"},
	{"in Spanish", "Elodia", "Spanish", "Hola, Elodia"},
	{"in French", "Antoniette", "French", "Bonjour, Antoniette"},
	{"in Czech", "František", "Czech", "Ahoj, František"},
}

func TestHello(t *testing.T) {
	assertCorrectMessage := func(t testing.TB, got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got %q want %q", got, want)
		}
	}
	for _, h := range helloTests {
		t.Run(h.title, func(t *testing.T) {
			got := Hello(h.name, h.lang)
			want := h.want
			assertCorrectMessage(t, got, want)
		})
	}
}
