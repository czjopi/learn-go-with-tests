package blogposts

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"io/fs"
	"strings"
)

type Post struct {
	Title, Description, Body string
	Tags                     []string
}

func NewPostsFromFS(filesystem fs.FS) ([]Post, error) {
	dir, err := fs.ReadDir(filesystem, ".")
	if err != nil {
		return nil, err
	}
	var posts []Post
	for _, file := range dir {
		post, err := getPost(filesystem, file.Name())
		if err != nil {
			return nil, err
		}
		posts = append(posts, post)
	}
	return posts, err
}

func getPost(filesystem fs.FS, filename string) (Post, error) {
	data, err := filesystem.Open(filename)
	if err != nil {
		return Post{}, err
	}
	defer data.Close()
	return newPost(data)
}

const (
	titleSeparator       = "Title: "
	descriptionSeparator = "Description: "
	tagsSeparator        = "Tags: "
)

func newPost(reader io.Reader) (Post, error) {
	scanner := bufio.NewScanner(reader)

	readMetaLine := func(tagName string) string {
		scanner.Scan()
		return strings.TrimPrefix(scanner.Text(), tagName)
	}

	readBody := func() string {
		scanner.Scan() // skip --- line
		buf := bytes.Buffer{}

		for scanner.Scan() {
			fmt.Fprintln(&buf, scanner.Text())
		}

		return strings.TrimSuffix(buf.String(), "\n")
	}

	return Post{
		Title:       readMetaLine(titleSeparator),
		Description: readMetaLine(descriptionSeparator),
		Tags:        strings.Split(readMetaLine(tagsSeparator), ", "),
		Body:        readBody(),
	}, nil
}
