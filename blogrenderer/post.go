package blogrenderer

import (
	"github.com/gomarkdown/markdown"
	"html/template"
	"strings"
)

// Post is representation of post
type Post struct {
	Title, Description, Body string
	Tags                     []string
}

// SanitizeTitle returns Post.Title in lower case with spaces replaced by dash
func (p Post) SanitizeTitle() string {
	return strings.ToLower(strings.Replace(p.Title, " ", "-", -1))
}

// MdBody return markdown body as html
func (p Post) MdBody() template.HTML {
	return template.HTML(markdown.ToHTML([]byte(p.Body), nil, nil))
}
