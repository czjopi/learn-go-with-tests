package main

// Sum returns sum of integers in a slice
func Sum(numbers []int) int {
	var sum int
	for _, number := range numbers {
		sum += number
	}
	return sum
}

// SumAll returns sum of sum of integers in a slices
func SumAll(numbers ...[]int) []int {
	var sum = []int{}
	for _, slice := range numbers {
		sum = append(sum, Sum(slice))
	}
	return sum
}

// SumAllTails returns sum of tails of integers in a slices
// tail is a slice without first item
func SumAllTails(numbers ...[]int) []int {
	var sum = []int{}
	for _, slice := range numbers {
		if len(slice) > 1 {
			tail := slice[1:]
			sum = append(sum, Sum(tail))
		} else {
			sum = append(sum, 0)
		}
	}
	return sum
}
