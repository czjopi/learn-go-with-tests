package clockface

import (
	"math"
	"testing"
	"time"
)

func TestSecondsInRadians(t *testing.T) {
	tests := []struct {
		name string
		time time.Time
		want float64
	}{
		{"thirty seconds", simpleTime(0, 0, 30), math.Pi},
		{"fifteen seconds", simpleTime(0, 0, 15), math.Pi / 2},
		{"forty-five seconds", simpleTime(0, 0, 45), (math.Pi / 2) * 3},
		{"zero seconds", simpleTime(0, 0, 0), 0},
		{"sixty seconds", simpleTime(0, 0, 60), 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			got := secondsInRadians(tt.time)

			if got != tt.want {
				t.Errorf("got %v radians want %v radians", got, tt.want)
			}
		})
	}
}

func TestSecondHandPoint(t *testing.T) {
	tests := []struct {
		time  time.Time
		point Point
	}{
		{simpleTime(0, 0, 30), Point{0, -1}},
		{simpleTime(0, 0, 45), Point{-1, 0}},
	}
	for _, tt := range tests {
		got := SecondHandPoint(tt.time)
		if !roughlyEqualPoint(tt.point, got) {
			t.Fatalf("Wanted %v Point, got %v.", tt.point, got)
		}
	}
}

func TestMinutesInRadians(t *testing.T) {
	tests := []struct {
		name string
		time time.Time
		want float64
	}{
		{"thirty minutes", simpleTime(0, 30, 0), math.Pi},
		{"fifteen minutes", simpleTime(0, 15, 0), math.Pi / 2},
		{"forty-five minutes", simpleTime(0, 45, 0), (math.Pi / 2) * 3},
		{"zero minutes", simpleTime(0, 0, 0), 0},
		{"sixty minutes", simpleTime(0, 60, 0), 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			got := minutesInRadians(tt.time)

			if got != tt.want {
				t.Errorf("got %v radians want %v radians", got, tt.want)
			}
		})
	}
}

func TestMinuteHandPoint(t *testing.T) {
	tests := []struct {
		time  time.Time
		point Point
	}{
		{simpleTime(0, 30, 0), Point{0, -1}},
		{simpleTime(0, 45, 0), Point{-1, 0}},
	}
	for _, tt := range tests {
		got := MinuteHandPoint(tt.time)
		if !roughlyEqualPoint(tt.point, got) {
			t.Fatalf("Wanted %v Point, got %v.", tt.point, got)
		}
	}
}

func TestHoursInRadians(t *testing.T) {
	tests := []struct {
		name string
		time time.Time
		want float64
	}{
		{"six hours", simpleTime(6, 0, 0), math.Pi},
		{"three hours", simpleTime(3, 0, 0), math.Pi / 2},
		{"nine hours", simpleTime(9, 0, 0), math.Pi * 1.5},
		{"zero hours", simpleTime(0, 0, 0), 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			got := hoursInRadians(tt.time)

			if got != tt.want {
				t.Errorf("got %v radians want %v radians", got, tt.want)
			}
		})
	}
}

func TestHoursHandPoint(t *testing.T) {
	tests := []struct {
		time  time.Time
		point Point
	}{
		{simpleTime(6, 0, 0), Point{0, -1}},
		{simpleTime(9, 0, 0), Point{-1, 0}},
	}
	for _, tt := range tests {
		got := HoursHandPoint(tt.time)
		if !roughlyEqualPoint(tt.point, got) {
			t.Fatalf("Wanted %v Point, got %v.", tt.point, got)
		}
	}
}

func simpleTime(hour, minute, second int) time.Time {
	return time.Date(1993, time.January, 15, hour, minute, second, 0, time.UTC)
}

func roughlyEqualPoint(a, b Point) bool {
	return roughlyEqualFloat64(a.X, b.X) &&
		roughlyEqualFloat64(a.Y, b.Y)
}

func roughlyEqualFloat64(x, x2 float64) bool {
	return math.Abs(x-x2) < 1e-7
}
