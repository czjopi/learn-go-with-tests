package main

import (
	"bytes"
	"testing"
)

func TestGreat(t *testing.T) {
	buffer := bytes.Buffer{}
	Greet(&buffer, "Tony")

	got := buffer.String()
	want := "Hello, Tony"

	if got != want {
		t.Errorf("got: %q want: %q", got, want)
	}
}
