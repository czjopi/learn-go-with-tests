package main

import (
	"fmt"
	"io"
	"os"
	"time"
)

const finalWord = "Go!"
const countDown = 3

// Sleeper allow you to put delay
type Sleeper interface {
	Sleep()
}

// ConfigurableSleeper is an implementation of Sleeper with
// a defined delay.
type ConfigurableSleeper struct {
	duration time.Duration
	sleep    func(time.Duration)
}

// Sleep will pause execution for the defined duration.
func (c *ConfigurableSleeper) Sleep() {
	c.sleep(c.duration)
}

// Countdown prints a countdown from 3 to output with a delay
// between count provided by Sleeper.
func Countdown(output io.Writer, s Sleeper) {
	for i := countDown; i > 0; i-- {
		fmt.Fprintln(output, i)
		time.Sleep(1 * time.Second)
		s.Sleep()
	}
	fmt.Fprint(output, finalWord)
}

func main() {
	sleeper := &ConfigurableSleeper{1 * time.Second, time.Sleep}
	Countdown(os.Stdout, sleeper)
}
