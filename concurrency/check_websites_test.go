package concurrency

import (
	"reflect"
	"testing"
)

func mockWebsiteChecker(url string) bool {
	if url == "hulahula.com" {
		return false
	}
	return true
}

func TestCheckWebsites(t *testing.T) {
	websites := []string{
		"hulahula.com",
		"netflix.com",
		"ripe.net",
		"he.net",
	}

	want := map[string]bool{
		"hulahula.com": false,
		"netflix.com":  true,
		"ripe.net":     true,
		"he.net":       true,
	}

	got := CheckWebsites(mockWebsiteChecker, websites)

	if !reflect.DeepEqual(got, want) {
		t.Errorf("got: %v want: %v", got, want)
	}
}
