package main

type Dictionary map[string]string

const (
	ErrStringNotFound   = DictionaryErr("could not find the key")
	ErrKeyExists        = DictionaryErr("cannot add key because key exists in dictionary")
	ErrKeyDoesNotExists = DictionaryErr("cannot update key because key doesn't exists in dictionary")
)

type DictionaryErr string

func (e DictionaryErr) Error() string {
	return string(e)
}

func (d Dictionary) Search(search string) (string, error) {
	str, ok := d[search]
	if ok {
		return str, nil
	}
	return str, ErrStringNotFound
}

func (d Dictionary) Add(key, value string) error {
	_, err := d.Search(key)

	switch err {
	case nil:
		return ErrKeyExists
	case ErrStringNotFound:
		d[key] = value
	default:
		return err
	}
	return nil
}

func (d Dictionary) Update(key, value string) error {
	_, err := d.Search(key)

	switch err {
	case nil:
		d[key] = value
	case ErrStringNotFound:
		d.Add(key, value)
	default:
		return err
	}
	return nil
}

func (d Dictionary) Delete(key string) {
	delete(d, key)
}
