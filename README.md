### Go Fundamentals

1. [Hello, world](hello-world) - Declaring variables, constants, if/else statements, switch, write your first go program and write your first test. Sub-test syntax and closures.
2. [Integers](integers) - Further Explore function declaration syntax and learn new ways to improve the documentation of your code.
3. [Iteration](iteration) - Learn about `for` and benchmarking.
4. [Arrays and slices](slices) - Learn about arrays, slices, `len`, varargs, `range` and test coverage.
5. [Structs, methods & interfaces](structs) - Learn about `struct`, methods, `interface` and table driven tests.
6. [Pointers & errors](pointers) - Learn about pointers and errors.
7. [Maps](maps) - Learn about storing values in the map data structure.
8. [Dependency Injection](di) - Learn about dependency injection, how it relates to using interfaces and a primer on io.
9. [Mocking](mocking) - Take some existing untested code and use DI with mocking to test it.
10. [Concurrency](concurrency) - Learn how to write concurrent code to make your software faster.
11. [Select](select) - Learn how to synchronise asynchronous processes elegantly.
12. [Reflection](reflection) - Learn about reflection
13. [Sync](sync) - Learn some functionality from the sync package including `WaitGroup` and `Mutex`
14. [Context](context) - Use the context package to manage and cancel long-running processes
15. [Property based tests](roman-numerals) - Practice some TDD with the Roman Numerals kata and get a brief intro to property based tests
16. [Math](math) - Use the `math` package to draw an SVG clock
17. [Reading Files](blogposts) - Read files and process them
18. [Templating](blogrenderer) - Use Go's `html/template` package to render `html` from data, and also learn about approval testing
19. [Generics](generics) - Learn how to write functions that take generic arguments and make your own generic data-structure