package blogposts_test

import (
	"blogposts"
	"reflect"
	"testing"
	"testing/fstest"
)

func TestNewBlogPosts(t *testing.T) {
	const (
		firstPost = `Title: title 1
Description: description 1
Tags: go, tdd
---
Hello world!

The body of post starts after ---`
		secondPost = `Title: title 2
Description: description 2,
Tags: python`
	)
	fs := fstest.MapFS{
		"hello_world.md": {Data: []byte(firstPost)},
		"hola.md":        {Data: []byte(secondPost)},
	}

	posts, err := blogposts.NewPostsFromFS(fs)
	if err != nil {
		t.Fatal(err)
	}

	assertPostsLen(t, len(posts), len(fs))

	assertPost(t, posts[0], blogposts.Post{
		Title:       "title 1",
		Description: "description 1",
		Tags:        []string{"go", "tdd"},
		Body: `Hello world!

The body of post starts after ---`,
	})
}

func assertPost(t *testing.T, got blogposts.Post, want blogposts.Post) {
	t.Helper()
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %+v, want %+v", got, want)
	}
}

func assertPostsLen(t *testing.T, lenPosts int, lenFiles int) {
	t.Helper()
	if lenPosts != lenFiles {
		t.Errorf("got %d posts, want %d posts.", lenPosts, lenFiles)
	}
}
