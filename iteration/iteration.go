package iteration

// Repeat returns character repeated 5 times.
func Repeat(character string, count int) string {
	if character == "" {
		return ""
	}

	var s string
	for i := 0; i < count; i++ {
		s += character
	}
	return s
}
