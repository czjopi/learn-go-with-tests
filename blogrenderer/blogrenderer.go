package blogrenderer

import (
	"embed"
	"html/template"
	"io"
)

var (
	//go:embed "templates"
	postTitle embed.FS
)

// PostRenderer renders post to HTML
type PostRenderer struct {
	templ *template.Template
}

// NewPostRenderer create new PostRenderer
func NewPostRenderer() (*PostRenderer, error) {
	tmpl, err := template.ParseFS(postTitle, "templates/*.gohtml")
	if err != nil {
		return nil, err
	}
	return &PostRenderer{templ: tmpl}, nil
}

// Render renders post into HTML
func (r *PostRenderer) Render(writter io.Writer, post Post) error {
	return r.templ.ExecuteTemplate(writter, "blog.gohtml", post)
}

// RenderIndex renders list of posts into HTML
func (r *PostRenderer) RenderIndex(writter io.Writer, posts []Post) error {
	return r.templ.ExecuteTemplate(writter, "index.gohtml", posts)
}
