package main

import (
	"sync"
	"testing"
)

func TestCounter(t *testing.T) {
	t.Run("Increment counter 3 times", func(t *testing.T) {
		counter := NewCounter()
		counter.Inc()
		counter.Inc()
		counter.Inc()

		want := 3

		assertCount(t, counter, want)
	})
	t.Run("Run safe concurrently", func(t *testing.T) {
		want := 1000
		counter := NewCounter()

		wg := sync.WaitGroup{}
		wg.Add(want)

		for i := 0; i < 1000; i++ {
			go func(c *Counter) {
				c.Inc()
				wg.Done()
			}(counter)
		}
		wg.Wait()

		assertCount(t, counter, want)
	})
}

func assertCount(t testing.TB, got *Counter, want int) {
	t.Helper()

	if got.Value() != want {
		t.Errorf("got %v want %v", got, want)
	}
}
