package numerals

import (
	"fmt"
	"log"
	"testing"
	"testing/quick"
)

var (
	tests = []struct {
		arabic int
		roman  string
	}{
		{1, "I"},
		{2, "II"},
		{3, "III"},
		{4, "IV"},
		{5, "V"},
		{6, "VI"},
		{7, "VII"},
		{8, "VIII"},
		{9, "IX"},
		{10, "X"},
		{14, "XIV"},
		{18, "XVIII"},
		{20, "XX"},
		{39, "XXXIX"},
		{40, "XL"},
		{47, "XLVII"},
		{49, "XLIX"},
		{50, "L"},
		{100, "C"},
		{90, "XC"},
		{400, "CD"},
		{500, "D"},
		{900, "CM"},
		{1000, "M"},
		{1984, "MCMLXXXIV"},
		{3999, "MMMCMXCIX"},
		{2014, "MMXIV"},
		{1006, "MVI"},
		{798, "DCCXCVIII"},
	}
)

func TestConvertToRoman(t *testing.T) {
	for _, tt := range tests {
		t.Run(fmt.Sprintf("%d converted to %s", tt.arabic, tt.roman), func(t *testing.T) {
			if got := ConvertToRoman(tt.arabic); got != tt.roman {
				t.Errorf("ConvertToRoman(%v) = %v, roman %v", tt.arabic, got, tt.roman)
			}
		})
	}
}

func TestConvertToArabic(t *testing.T) {
	for _, tt := range tests {
		t.Run(fmt.Sprintf("%s converted to %d", tt.roman, tt.arabic), func(t *testing.T) {
			if got := ConvertToArabic(tt.roman); got != tt.arabic {
				t.Errorf("ConvertToArabic(%v) = %v, arabic %v", tt.roman, got, tt.arabic)
			}
		})
	}
}

func TestPropertiesOfConversion(t *testing.T) {
	assertion := func(arabic int) bool {
		if arabic < 0 || arabic > 3999 {
			log.Println(arabic)
			return true
		}
		roman := ConvertToRoman(arabic)
		fromRoman := ConvertToArabic(roman)
		return fromRoman == arabic
	}
	if err := quick.Check(assertion, &quick.Config{MaxCount: 10000}); err != nil {
		t.Error("failed checks", err)
	}
}
