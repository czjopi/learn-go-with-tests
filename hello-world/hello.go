package main

import "fmt"

const EnglishHelloPrefix = "Hello, "
const FrenchHelloPrefix = "Bonjour, "
const SpanishHelloPrefix = "Hola, "
const CzechHelloPrefix = "Ahoj, "

func Hello(name, language string) string {
	if name == "" {
		name = "World"
	}
	return greetingPrefix(language) + name
}

func greetingPrefix(language string) (prefix string) {
	switch language {
	case "Spanish":
		prefix = SpanishHelloPrefix
	case "French":
		prefix = FrenchHelloPrefix
	case "Czech":
		prefix = CzechHelloPrefix
	default:
		prefix = EnglishHelloPrefix
	}
	return
}

func main() {
	fmt.Println(Hello("Chris", ""))
}
